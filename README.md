Recurrent Models - analysis of sentiment classification. Tasks:

1 Loading data from Stanford Sentiment Treebank dataset

2 Baseline model implementation

3 Recurrent neural network implementation

4 Models comparison and hyperparameters validation

Implemented in Python using NumPy, PyTorch and Matplotlib library.

My lab assignment in Deep Learning, FER, Zagreb.

Created: 2021
