import torch
import numpy as np
import torch.nn as nn
from train import load_embed_mat, load_vocab, load_dataset, train, evaluate
from rnn_compare import RNNModel
from baseline import BaselineModel
from torch.optim import Adam, SGD, Adagrad, RMSprop
import argparse

def trainValTest(model, lr, epochs, train_dataset, batch_size_train, grad_clip, valid_dataset, batch_size_val, test_dataset, batch_size_test, optimiz=Adam):
    criterion = nn.BCEWithLogitsLoss()
    optimizer = optimiz(model.parameters(), lr=lr)

    for epoch in range(1, epochs + 1):
        print()
        print()
        print("################ EPOCH {}: ################".format(epoch))
        print("------ TRAIN ------")
        train(model, train_dataset, batch_size_train, optimizer, criterion, grad_clip)
        print()
        print("------ VAL ------")
        evaluate(model, valid_dataset, batch_size_val, criterion)

    print()
    print("************** TEST **************")
    evaluate(model, test_dataset, batch_size_test, criterion)


def initialize_model(text_vocab, rnn_type, hidden_size=150, num_layers=2, dropout=0, bidirectional=False, pretrained=True, activ_fun=torch.relu):
    print("rnn_type = {}, hidden_size = {}, num_layers = {}, dropout = {}, bidirectional = {}".format(rnn_type, hidden_size, num_layers, dropout, bidirectional))
    embed_mat = load_embed_mat(text_vocab, pretrained=pretrained)
    # embed_mat = load_embed_mat(text_vocab, pretrained=False)

    return RNNModel(embed_mat, rnn_type, hidden_size, num_layers, dropout, bidirectional, activ_fun=activ_fun)

def initialize_baseline_model(text_vocab, pretrained=True, activ_fun=torch.relu):
    print("baseline model")
    embed_mat = load_embed_mat(text_vocab, pretrained=pretrained)

    return BaselineModel(embed_mat, activ_fun=activ_fun)


def main(args, run_wo_pretrained, run_hp_valid, run_best_rnns, best_rnns_hp_hidsiz_numlay_dropout_bidir, best_rnn_base_hps,
         max_size_list = [50, 6000, 12000], lr_list = [1e-3, 1e-2, 1e-1], optimizer_list = [SGD, Adagrad, RMSprop], activ_fun_list = [torch.sigmoid, torch.tanh, torch.rrelu], grad_clip_list = [0.1, 1, 10],
         max_size = -1, lr = 1e-4, optimizer=Adam, activ_fun=torch.relu, grad_clip = 0.25):

    seed = int(args.seed)
    max_size = int(args.maxsize) if not run_hp_valid else max_size
    min_freq = int(args.minfreq)
    epochs = int(args.epochs)
    lr = float(args.lr) if not run_hp_valid else lr
    batch_size_train, batch_size_val, batch_size_test = int(args.bs_train), int(args.bs_val), int(args.bs_test)
    if not run_hp_valid:
        grad_clip = float(args.grad_clip) if args.grad_clip else None
    print("grad_clip", grad_clip)


    np.random.seed(seed)
    torch.manual_seed(seed)

    ##############

    if run_wo_pretrained:
        text_vocab, label_vocab = load_vocab("data/sst_train_raw.csv", max_size, min_freq)

        train_dataset, valid_dataset, test_dataset = load_dataset("data/sst_train_raw.csv", "data/sst_valid_raw.csv",
                                                                  "data/sst_test_raw.csv", text_vocab, label_vocab)

        print("======== PRETRAINED = TRUE")
        for best_hps in best_rnns_hp_hidsiz_numlay_dropout_bidir:
            model = initialize_model(text_vocab, *best_hps, pretrained=True)

            trainValTest(model, lr, epochs, train_dataset, batch_size_train, grad_clip, valid_dataset, batch_size_val,
                         test_dataset, batch_size_test)

            print()
            print()

        baseline_model = initialize_baseline_model(text_vocab, pretrained=True)
        trainValTest(baseline_model, lr, epochs, train_dataset, batch_size_train, grad_clip, valid_dataset,
                     batch_size_val, test_dataset, batch_size_test)

        print()
        print()
        print()
        print()
        print("======== PRETRAINED = FALSE")
        for best_hps in best_rnns_hp_hidsiz_numlay_dropout_bidir:
            model = initialize_model(text_vocab, *best_hps, pretrained=False)

            trainValTest(model, lr, epochs, train_dataset, batch_size_train, grad_clip, valid_dataset, batch_size_val, test_dataset, batch_size_test)

            print()
            print()

        baseline_model = initialize_baseline_model(text_vocab, pretrained=False)
        trainValTest(baseline_model, lr, epochs, train_dataset, batch_size_train, grad_clip, valid_dataset, batch_size_val, test_dataset, batch_size_test)

    if run_hp_valid:
        best_rnn_hps = best_rnns_hp_hidsiz_numlay_dropout_bidir[1]   # best model = GRU
        print("best_rnn_hps:",best_rnn_hps)

        # VALIDATE max_size
        print()
        print()
        print("------- VALIDATE max_size -------")
        for max_size_valid in max_size_list:
            text_vocab, label_vocab = load_vocab("data/sst_train_raw.csv", max_size_valid, min_freq)

            train_dataset, valid_dataset, test_dataset = load_dataset("data/sst_train_raw.csv", "data/sst_valid_raw.csv",
                                                                      "data/sst_test_raw.csv", text_vocab, label_vocab)

            print("max_size = {}, lr = {}, optimizer = {}, activ_fun = {}, grad_clip = {}".format(max_size_valid, lr, optimizer, activ_fun, grad_clip))

            print()
            print(":::::: GRU ::::::")
            model = initialize_model(text_vocab, *best_rnn_hps, pretrained=True)
            trainValTest(model, lr, epochs, train_dataset, batch_size_train, grad_clip, valid_dataset, batch_size_val, test_dataset, batch_size_test)

            print()
            print(":::::: BASELINE ::::::")
            baseline_model = initialize_baseline_model(text_vocab, pretrained=True)
            trainValTest(baseline_model, lr, epochs, train_dataset, batch_size_train, grad_clip, valid_dataset, batch_size_val, test_dataset, batch_size_test)


        # same Vocab in VALIDATE lr, VALIDATE optimizer, VALIDATE activ_fun, VALIDATE grad_clip
        text_vocab, label_vocab = load_vocab("data/sst_train_raw.csv", max_size, min_freq)

        train_dataset, valid_dataset, test_dataset = load_dataset("data/sst_train_raw.csv", "data/sst_valid_raw.csv",
                                                                  "data/sst_test_raw.csv", text_vocab, label_vocab)

        # VALIDATE lr
        print()
        print()
        print("------- VALIDATE lr -------")
        for lr_valid in lr_list:
            print("max_size = {}, lr = {}, optimizer = {}, activ_fun = {}, grad_clip = {}".format(max_size, lr_valid, optimizer, activ_fun, grad_clip))

            print()
            print(":::::: GRU ::::::")
            model = initialize_model(text_vocab, *best_rnn_hps, pretrained=True)
            trainValTest(model, lr_valid, epochs, train_dataset, batch_size_train, grad_clip, valid_dataset, batch_size_val,
                         test_dataset, batch_size_test)

            print()
            print(":::::: BASELINE ::::::")
            baseline_model = initialize_baseline_model(text_vocab, pretrained=True)
            trainValTest(baseline_model, lr_valid, epochs, train_dataset, batch_size_train, grad_clip, valid_dataset,
                         batch_size_val, test_dataset, batch_size_test)

        # VALIDATE optimizer
        print()
        print()
        print("------- VALIDATE optimizer -------")
        for optimizer_valid in optimizer_list:
            print("max_size = {}, lr = {}, optimizer = {}, activ_fun = {}, grad_clip = {}".format(max_size, lr, optimizer_valid, activ_fun, grad_clip))

            print()
            print(":::::: GRU ::::::")
            model = initialize_model(text_vocab, *best_rnn_hps, pretrained=True)
            trainValTest(model, lr, epochs, train_dataset, batch_size_train, grad_clip, valid_dataset, batch_size_val,
                         test_dataset, batch_size_test, optimiz=optimizer_valid)

            print()
            print(":::::: BASELINE ::::::")
            baseline_model = initialize_baseline_model(text_vocab, pretrained=True)
            trainValTest(baseline_model, lr, epochs, train_dataset, batch_size_train, grad_clip, valid_dataset,
                         batch_size_val, test_dataset, batch_size_test, optimiz=optimizer_valid)

        # VALIDATE activ_fun
        print()
        print()
        print("------- VALIDATE activ_fun -------")
        for activ_fun_valid in activ_fun_list:
            print("max_size = {}, lr = {}, optimizer = {}, activ_fun = {}, grad_clip = {}".format(max_size, lr, optimizer, activ_fun_valid, grad_clip))

            print()
            print(":::::: GRU ::::::")
            model = initialize_model(text_vocab, *best_rnn_hps, pretrained=True, activ_fun=activ_fun_valid)
            trainValTest(model, lr, epochs, train_dataset, batch_size_train, grad_clip, valid_dataset, batch_size_val,
                         test_dataset, batch_size_test)

            print()
            print(":::::: BASELINE ::::::")
            baseline_model = initialize_baseline_model(text_vocab, pretrained=True, activ_fun=activ_fun_valid)
            trainValTest(baseline_model, lr, epochs, train_dataset, batch_size_train, grad_clip, valid_dataset,
                         batch_size_val, test_dataset, batch_size_test)

        # VALIDATE grad_clip
        print()
        print()
        print("------- VALIDATE grad_clip -------")
        for grad_clip_valid in grad_clip_list:
            print("max_size = {}, lr = {}, optimizer = {}, activ_fun = {}, grad_clip = {}".format(max_size, lr, optimizer, activ_fun, grad_clip_valid))

            print()
            print(":::::: GRU ::::::")
            model = initialize_model(text_vocab, *best_rnn_hps, pretrained=True)
            trainValTest(model, lr, epochs, train_dataset, batch_size_train, grad_clip_valid, valid_dataset, batch_size_val,
                         test_dataset, batch_size_test)

            print()
            print(":::::: BASELINE ::::::")
            baseline_model = initialize_baseline_model(text_vocab, pretrained=True)
            trainValTest(baseline_model, lr, epochs, train_dataset, batch_size_train, grad_clip_valid, valid_dataset,
                         batch_size_val, test_dataset, batch_size_test)

    if run_best_rnns:
        best_rnn_hps = best_rnns_hp_hidsiz_numlay_dropout_bidir[1]   # best model = GRU
        print("best_rnn_hps:",best_rnn_hps)

        rnn_best_hps, baseline_best_hps = best_rnn_base_hps
        print("rnn_best_hps:", rnn_best_hps)
        print("baseline_best_hps:", baseline_best_hps)

        print()

        # train best RNN with best hps ({"max_size":-1, "lr":1e-4, "optimizer":Adam, "activ_fun":torch.rrelu, "grad_clip":0.25})
        text_vocab, label_vocab = load_vocab("data/sst_train_raw.csv", rnn_best_hps["max_size"], min_freq)

        train_dataset, valid_dataset, test_dataset = load_dataset("data/sst_train_raw.csv", "data/sst_valid_raw.csv",
                                                                  "data/sst_test_raw.csv", text_vocab, label_vocab)

        print("max_size = {}, lr = {}, optimizer = {}, activ_fun = {}, grad_clip = {}".format(rnn_best_hps["max_size"], rnn_best_hps["lr"],
                                                                                              rnn_best_hps["optimizer"], rnn_best_hps["activ_fun"],
                                                                                              rnn_best_hps["grad_clip"]))

        print()
        model = initialize_model(text_vocab, *best_rnn_hps, pretrained=True, activ_fun=rnn_best_hps["activ_fun"])
        trainValTest(model, rnn_best_hps["lr"], epochs, train_dataset, batch_size_train, rnn_best_hps["grad_clip"], valid_dataset, batch_size_val,
                     test_dataset, batch_size_test, optimiz=rnn_best_hps["optimizer"])

        print()
        print()
        print()
        print()

        # train baseline model with best hps ({"max_size":-1, "lr":1e-4, "optimizer":Adam, "activ_fun":torch.relu, "grad_clip":10})
        text_vocab, label_vocab = load_vocab("data/sst_train_raw.csv", baseline_best_hps["max_size"], min_freq)

        train_dataset, valid_dataset, test_dataset = load_dataset("data/sst_train_raw.csv", "data/sst_valid_raw.csv",
                                                                  "data/sst_test_raw.csv", text_vocab, label_vocab)

        print("max_size = {}, lr = {}, optimizer = {}, activ_fun = {}, grad_clip = {}".format(baseline_best_hps["max_size"], baseline_best_hps["lr"],
                                                                                              baseline_best_hps["optimizer"], baseline_best_hps["activ_fun"],
                                                                                              baseline_best_hps["grad_clip"]))

        print()
        baseline_model = initialize_baseline_model(text_vocab, pretrained=True, activ_fun=baseline_best_hps["activ_fun"])
        trainValTest(baseline_model, baseline_best_hps["lr"], epochs, train_dataset, batch_size_train, baseline_best_hps["grad_clip"], valid_dataset,
                     batch_size_val, test_dataset, batch_size_test, optimiz=baseline_best_hps["optimizer"])





def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--seed")
    parser.add_argument("--maxsize")
    parser.add_argument("--minfreq")
    parser.add_argument("--epochs")
    parser.add_argument("--lr")
    parser.add_argument("--bs_train")
    parser.add_argument("--bs_val")
    parser.add_argument("--bs_test")
    parser.add_argument("--grad_clip")

    return parser.parse_args()


if __name__=="__main__":
    run_wo_pretrained = False
    run_hp_valid = False
    run_best_rnns = True

    best_rnns_hp_hidsiz_numlay_dropout_bidir = [(nn.RNN, 150, 2, 0, False),
                             (nn.GRU, 150, 2, 0, False),
                             (nn.LSTM, 150, 1, 0, False)]

    # tuple: (best GRU hyperparams, best baseline model hyperparams)
    best_rnn_base_hps = ({"max_size":-1, "lr":1e-4, "optimizer":Adam, "activ_fun":torch.rrelu, "grad_clip":0.25},
                        {"max_size":-1, "lr":1e-4, "optimizer":Adam, "activ_fun":torch.relu, "grad_clip":10})


    main(parse_args(), run_wo_pretrained, run_hp_valid, run_best_rnns, best_rnns_hp_hidsiz_numlay_dropout_bidir, best_rnn_base_hps)