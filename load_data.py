from dataclasses import dataclass
from torch.utils.data import Dataset, DataLoader
from torch.nn.utils.rnn import pad_sequence
import torch
import csv


class NLPDataset(Dataset):

    def __init__(self, ds_filename, text_vocab, label_vocab):
        self.text_vocab = text_vocab
        self.label_vocab = label_vocab
        self.instances = []
        self.from_file(ds_filename)

    def from_file(self, ds_filename):
        self.instances = get_instances(ds_filename)

    def __getitem__(self, item):
        inst = self.instances[item]

        return torch.tensor(self.text_vocab.encode(inst[0])), torch.tensor(self.label_vocab.encode(inst[1]))

    def __len__(self):
        return len(self.instances)


def pad_collate_fn(batch, pad_index=0):
    texts, labels = zip(*batch)

    orig_lens = torch.tensor([len(text) for text in texts])

    texts_pad = pad_sequence(texts, batch_first=True, padding_value=pad_index)

    return texts_pad, labels, orig_lens


class Vocab():
    def __init__(self, frequencies, max_size=-1, min_freq=0, txt_vocab=True):
        assert min_freq >= 0
        if txt_vocab:
            assert max_size == -1 or max_size >= 2
        else:
            assert max_size == -1 or max_size >= 1

        self.max_size = max_size
        self.min_freq = min_freq

        frequencies = [f for f in frequencies.items() if f[1] >= min_freq]

        freqs_sort = sorted([f for f in frequencies], key=lambda f: f[1], reverse=True)

        if max_size != -1:
            if txt_vocab:
                freqs_sort = freqs_sort[:max_size - 2]
            else:
                freqs_sort = freqs_sort[:max_size]

        self.itos, self.stoi = {}, {}

        if txt_vocab:
            self.itos[0] = "<PAD>"
            self.itos[1] = "<UNK>"
            self.stoi["<PAD>"] = 0
            self.stoi["<UNK>"] = 1

            for i in range(len(freqs_sort)):
                f = freqs_sort[i]
                self.itos[i + 2] = f[0]
                self.stoi[f[0]] = i + 2
        else:
            for i in range(len(freqs_sort)):
                f = freqs_sort[i]
                self.itos[i] = f[0]
                self.stoi[f[0]] = i


    def encode(self, tokens):
        if isinstance(tokens, list):
            return [self.stoi.get(t, self.stoi["<UNK>"]) for t in tokens]
        else:
            return self.stoi[tokens]


def get_embed_mat(vocab, embed_mat_filename=None):
    v_itos = vocab.itos
    V, d = len(v_itos), 300
    embed_mat = torch.randn(V, d)

    embed_mat[0] = torch.zeros(d)  # <PAD> vector representation

    token_to_dims = {}
    if embed_mat_filename:
        with open(embed_mat_filename, "r") as embed_mat_file:
            lines = embed_mat_file.readlines()
            for line in lines:
                tokens = line.split()
                token = tokens[0]
                dims = list(map(float, tokens[1:]))

                token_to_dims[token] = dims


    if embed_mat_filename:
        for i in range(V):
            dims = token_to_dims.get(v_itos[i], None)

            if dims:
                embed_mat[i] = torch.tensor(token_to_dims[v_itos[i]])

    return embed_mat


def get_embed_mat_wrapped(embed_mat, pretrained):
    return torch.nn.Embedding.from_pretrained(embed_mat, freeze=pretrained, padding_idx=0)


def get_instances(ds_instances_filename):
    with open(ds_instances_filename, "r") as ds_inst_file:
        ds_instances = []

        ds_file_reader = csv.reader(ds_inst_file)
        for line in ds_file_reader:
            ds_instances.append([line[0].strip().split(), line[1].strip()])

        return ds_instances


def get_word_and_label_frequencies(ds_instances_filename):
    ds_instances = get_instances(ds_instances_filename=ds_instances_filename)

    word_to_freq, label_to_freq = {}, {}

    for inst in ds_instances:
        words, label = inst[0], inst[1]
        for w in words:
            word_to_freq[w] = word_to_freq.get(w, 0) + 1
        label_to_freq[label] = label_to_freq.get(label, 0) + 1

    return word_to_freq, label_to_freq


if __name__ == "__main__":

    word_frequencies, label_frequencies = get_word_and_label_frequencies("data/sst_train_raw.csv")
    text_vocab = Vocab(frequencies=word_frequencies, max_size=-1, min_freq=0, txt_vocab=True)
    label_vocab = Vocab(frequencies=label_frequencies, max_size=-1, min_freq=0, txt_vocab=False)

    train_dataset = NLPDataset("data/sst_train_raw.csv", text_vocab, label_vocab)

    batch_size = 2  # Only for demonstrative purposes
    shuffle = False  # Only for demonstrative purposes
    train_dataloader = DataLoader(dataset=train_dataset, batch_size=batch_size,
                                  shuffle=shuffle, collate_fn=pad_collate_fn)
    texts, labels, lengths = next(iter(train_dataloader))
    print(f"Texts: {texts}")
    print(f"Labels: {labels}")
    print(f"Lengths: {lengths}")

    print(" EMBED MATRIX ")
    embed_mat = get_embed_mat(vocab=text_vocab,embed_mat_filename="data/sst_glove_6b_300d.txt")
    print(embed_mat.shape)
    print(embed_mat)
    print(get_embed_mat_wrapped(embed_mat, pretrained=False))
