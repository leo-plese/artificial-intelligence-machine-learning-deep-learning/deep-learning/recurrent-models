import torch
import numpy as np
import torch.nn as nn
from train import load_embed_mat, load_vocab, load_dataset, train, evaluate
from torch.optim import Adam
import argparse


class RNNModel(nn.Module):
    def __init__(self, embed_mat, rnn_model, hidden_size, num_layers, dropout, bidirectional, activ_fun=torch.relu):
        super().__init__()

        self.embed_mat = embed_mat

        self.rnn1 = rnn_model(input_size=300, hidden_size=hidden_size, num_layers=num_layers, batch_first=False, dropout=dropout, bidirectional=bidirectional)
        self.rnn2 = rnn_model(input_size=(hidden_size * 2 if bidirectional else hidden_size), hidden_size=hidden_size, num_layers=num_layers, batch_first=False, dropout=dropout, bidirectional=bidirectional)

        self.fc1 = nn.Linear((hidden_size*2 if bidirectional else hidden_size), 150, bias=True)
        self.fc2 = nn.Linear(150, 1, bias=True)

        self.activ_fun = activ_fun

    def forward(self, x):
        x = self.embed_mat(x)
        y = x.transpose(1,0)

        # y - all hidden states from last layer
        # (h_n, c_n) - hidden/cell state for each layer at t = seq_len
        # implicitly (default): (h_0, c_0) = (torch.zeros(2,10,150), torch.zeros(2,10,150))
        y, hc = self.rnn1(y)
        y, hc = self.rnn2(y, hc)

        # input to decoder = output of last layer (layer 2) from last RNN cell
        y = y[-1, ...]
        y = self.fc1(y)
        # y = y.relu()
        y = self.activ_fun(y)
        y = self.fc2(y)

        return y

def initialize_model(text_vocab, rnn_type, hidden_size=150, num_layers=2, dropout=0, bidirectional=False):
    print("rnn_type = {}, hidden_size = {}, num_layers = {}, dropout = {}, bidirectional = {}".format(rnn_type, hidden_size, num_layers, dropout, bidirectional))
    embed_mat = load_embed_mat(text_vocab, pretrained=True)
    # embed_mat = load_embed_mat(text_vocab, pretrained=False)

    return RNNModel(embed_mat, rnn_type, hidden_size, num_layers, dropout, bidirectional)

def trainValTest(model, lr, epochs, train_dataset, batch_size_train, grad_clip, valid_dataset, batch_size_val, test_dataset, batch_size_test):
    criterion = nn.BCEWithLogitsLoss()
    optimizer = Adam(model.parameters(), lr=lr)

    for epoch in range(1, epochs + 1):
        print()
        print()
        print("################ EPOCH {}: ################".format(epoch))
        print("------ TRAIN ------")
        train(model, train_dataset, batch_size_train, optimizer, criterion, grad_clip)
        print()
        print("------ VAL ------")
        evaluate(model, valid_dataset, batch_size_val, criterion)

    print()
    print("************** TEST **************")
    evaluate(model, test_dataset, batch_size_test, criterion)



def main(args, run_other_rnns, run_rnn_comp, run_best_rnns, best_rnns_hyperparams,
         rnn_type_list = [nn.RNN, nn.GRU, nn.LSTM], hidden_size_list = [30, 150, 750], num_layers_list = [1, 2, 4, 8], dropout_list = [0, 0.5, 0.9], bidirectional_list = [True, False]):

    seed = int(args.seed)
    max_size, min_freq = int(args.maxsize), int(args.minfreq)
    epochs = int(args.epochs)
    lr = float(args.lr)
    batch_size_train, batch_size_val, batch_size_test = int(args.bs_train), int(args.bs_val), int(args.bs_test)
    grad_clip = float(args.grad_clip) if args.grad_clip else None
    print("grad_clip", grad_clip)

    np.random.seed(seed)
    torch.manual_seed(seed)

    text_vocab, label_vocab = load_vocab("data/sst_train_raw.csv", max_size, min_freq)

    train_dataset, valid_dataset, test_dataset = load_dataset("data/sst_train_raw.csv", "data/sst_valid_raw.csv",
                                                              "data/sst_test_raw.csv", text_vocab, label_vocab)


    if run_other_rnns:
        for rnn_type in rnn_type_list:
            print()
            print()
            print("***************** RNN type:", rnn_type, "*****************")
            model = initialize_model(text_vocab, rnn_type)

            trainValTest(model, lr, epochs, train_dataset, batch_size_train, grad_clip, valid_dataset, batch_size_val,
                         test_dataset, batch_size_test)


    if run_rnn_comp:
        for rnn_type in rnn_type_list:
            print()
            print()
            print()
            print()
            print("***************** RNN type:", rnn_type, "*****************")

            # VALIDATE hidden_size
            print()
            print()
            print("------- VALIDATE hidden_size -------")
            for hidden_size in hidden_size_list:
                print()
                print(":::: hidden_size =", hidden_size)

                model = initialize_model(text_vocab, rnn_type, hidden_size=hidden_size)
                #
                trainValTest(model, lr, epochs, train_dataset, batch_size_train, grad_clip, valid_dataset, batch_size_val, test_dataset, batch_size_test)

            # VALIDATE num_layers
            print()
            print()
            print("------- VALIDATE num_layers -------")
            for num_layers in num_layers_list:
                print()
                print(":::: num_layers =", num_layers)

                model = initialize_model(text_vocab, rnn_type, num_layers=num_layers)

                trainValTest(model, lr, epochs, train_dataset, batch_size_train, grad_clip, valid_dataset, batch_size_val, test_dataset, batch_size_test)

            # VALIDATE dropout
            print()
            print()
            print("------- VALIDATE dropout -------")
            for dropout in dropout_list:
                print()
                print(":::: dropout =", dropout)

                model = initialize_model(text_vocab, rnn_type, dropout=dropout)
                #
                trainValTest(model, lr, epochs, train_dataset, batch_size_train, grad_clip, valid_dataset, batch_size_val, test_dataset, batch_size_test)

            # VALIDATE bidirectional
            print()
            print()
            print("------- VALIDATE bidirectional -------")
            for bidirectional in bidirectional_list:
                print()
                print(":::: bidirectional =", bidirectional)

                model = initialize_model(text_vocab, rnn_type, bidirectional=bidirectional)
                #
                trainValTest(model, lr, epochs, train_dataset, batch_size_train, grad_clip, valid_dataset, batch_size_val, test_dataset, batch_size_test)

    if run_best_rnns:
        for best_hps in best_rnns_hyperparams:
            model = initialize_model(text_vocab, *best_hps)
            #
            trainValTest(model, lr, epochs, train_dataset, batch_size_train, grad_clip, valid_dataset, batch_size_val,
                         test_dataset, batch_size_test)



def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--seed")
    parser.add_argument("--maxsize")
    parser.add_argument("--minfreq")
    parser.add_argument("--epochs")
    parser.add_argument("--lr")
    parser.add_argument("--bs_train")
    parser.add_argument("--bs_val")
    parser.add_argument("--bs_test")
    parser.add_argument("--grad_clip")

    return parser.parse_args()



if __name__=="__main__":
    run_other_rnns = False
    run_rnn_comp = False
    run_best_rnns = True

    best_rnns_hyperparams = [(nn.RNN, 150, 2, 0, False),
                             (nn.GRU, 150, 2, 0, False),
                             (nn.LSTM, 150, 1, 0, False)]

    main(parse_args(),run_other_rnns, run_rnn_comp, run_best_rnns, best_rnns_hyperparams)
