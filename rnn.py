import torch
import numpy as np
import torch.nn as nn
from train import load_embed_mat, load_vocab, load_dataset, train, evaluate
from torch.optim import Adam
import argparse


class LSTMModel(nn.Module):
    def __init__(self, embed_mat):
        super().__init__()

        self.embed_mat = embed_mat

        self.lstm1 = nn.LSTM(input_size=300, hidden_size=150, num_layers=2, batch_first=False, bidirectional=False)
        self.lstm2 = nn.LSTM(input_size=150, hidden_size=150, num_layers=2, batch_first=False, bidirectional=False)

        self.fc1 = nn.Linear(150, 150, bias=True)
        self.fc2 = nn.Linear(150, 1, bias=True)

    def forward(self, x):
        x = self.embed_mat(x)
        y = x.transpose(1,0)

        # y - all hidden states from last layer
        # (h_n, c_n) - hidden/cell state for each layer at t = seq_len
        # implicitly (default): (h_0, c_0) = (torch.zeros(2,10,150), torch.zeros(2,10,150))
        y, hc = self.lstm1(y)
        y, hc = self.lstm2(y, hc)

        # input to decoder = output of last layer (layer 2) from last RNN cell
        y = y[-1, ...]
        y = self.fc1(y)
        y = y.relu()
        y = self.fc2(y)

        return y

def initialize_model(embed_mat):
    return LSTMModel(embed_mat)


def main(args):
    seed = int(args.seed)
    max_size, min_freq = int(args.maxsize), int(args.minfreq)
    epochs = int(args.epochs)
    lr = float(args.lr)
    batch_size_train, batch_size_val, batch_size_test = int(args.bs_train), int(args.bs_val), int(args.bs_test)
    grad_clip = float(args.grad_clip) if args.grad_clip else None
    print("grad_clip", grad_clip)

    np.random.seed(seed)
    torch.manual_seed(seed)

    text_vocab, label_vocab = load_vocab("data/sst_train_raw.csv", max_size, min_freq)

    embed_mat = load_embed_mat(text_vocab, pretrained=True)

    train_dataset, valid_dataset, test_dataset = load_dataset("data/sst_train_raw.csv", "data/sst_valid_raw.csv",
                                                              "data/sst_test_raw.csv", text_vocab, label_vocab)
    model = initialize_model(embed_mat)

    criterion = nn.BCEWithLogitsLoss()
    optimizer = Adam(model.parameters(), lr=lr)

    for epoch in range(1, epochs + 1):
        print()
        print()
        print("################ EPOCH {}: ################".format(epoch))
        print("------ TRAIN ------")
        train(model, train_dataset, batch_size_train, optimizer, criterion, grad_clip)
        print()
        print("------ VAL ------")
        evaluate(model, valid_dataset, batch_size_val, criterion)

    print()
    print("************** TEST **************")
    evaluate(model, test_dataset, batch_size_test, criterion)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--seed")
    parser.add_argument("--maxsize")
    parser.add_argument("--minfreq")
    parser.add_argument("--epochs")
    parser.add_argument("--lr")
    parser.add_argument("--bs_train")
    parser.add_argument("--bs_val")
    parser.add_argument("--bs_test")
    parser.add_argument("--grad_clip")

    return parser.parse_args()



if __name__=="__main__":
    main(parse_args())
