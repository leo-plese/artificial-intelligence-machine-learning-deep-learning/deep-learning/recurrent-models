import torch
import torch.nn as nn
import numpy as np
from train import load_embed_mat, load_vocab, load_dataset, train, evaluate
from torch.optim import Adam
import argparse


class BaselineModel(nn.Module):
    def __init__(self, embed_mat, activ_fun=torch.relu):
        super().__init__()

        self.embed_mat = embed_mat
        self.fc1 = nn.Linear(300, 150, bias=True)
        self.fc2 = nn.Linear(150, 150, bias=True)
        self.fc3 = nn.Linear(150, 1, bias=True)

        self.activ_fun = activ_fun

    def forward(self, x):
        # embed mat: V x d (14806 x 300)
        x = self.embed_mat(x)
        # x: B x T x d (e.g. train: 10 x 22 x 300)
        y = torch.mean(x, dim=1, keepdim=False)

        y = self.fc1(y)
        # y = y.relu()
        y =  self.activ_fun(y)
        y = self.fc2(y)
        # y = y.relu()
        y = self.activ_fun(y)
        y = self.fc3(y)
        return y


def initialize_model(embed_mat):
    return BaselineModel(embed_mat)


def main(args):
    seed = int(args.seed)
    max_size, min_freq = int(args.maxsize), int(args.minfreq)
    epochs = int(args.epochs)
    lr = float(args.lr)
    batch_size_train, batch_size_val, batch_size_test = int(args.bs_train), int(args.bs_val), int(args.bs_test)

    np.random.seed(seed)
    torch.manual_seed(seed)

    text_vocab, label_vocab = load_vocab("data/sst_train_raw.csv", max_size, min_freq)

    embed_mat = load_embed_mat(text_vocab, pretrained=True)

    train_dataset, valid_dataset, test_dataset = load_dataset("data/sst_train_raw.csv", "data/sst_valid_raw.csv",
                                                              "data/sst_test_raw.csv", text_vocab, label_vocab)
    model = initialize_model(embed_mat)

    criterion = nn.BCEWithLogitsLoss()
    optimizer = Adam(model.parameters(), lr=lr)

    for epoch in range(1, epochs + 1):
        print()
        print()
        print("################ EPOCH {}: ################".format(epoch))
        print("------ TRAIN ------")
        train(model, train_dataset, batch_size_train, optimizer, criterion)
        print()
        print("------ VAL ------")
        evaluate(model, valid_dataset, batch_size_val, criterion)

    print()
    print("************** TEST **************")
    evaluate(model, test_dataset, batch_size_test, criterion)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--seed")
    parser.add_argument("--maxsize")
    parser.add_argument("--minfreq")
    parser.add_argument("--epochs")
    parser.add_argument("--lr")
    parser.add_argument("--bs_train")
    parser.add_argument("--bs_val")
    parser.add_argument("--bs_test")

    return parser.parse_args()



if __name__=="__main__":
    main(parse_args())
